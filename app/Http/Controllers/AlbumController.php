<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Artist;

class AlbumController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $albums = Album::orderBy('album_name')->get();
        return view('album/index', compact('albums'));
    }

    public function create()
    {
        $artists = Artist::orderBy('artist_name')->get();
        return view('album/create', compact('artists'));
    }

    public function store(Request $request)
    {
        Album::create($request->all());
            session()->flash('status-success', 'Record created successfully!');
            return redirect()->route('album-create');
            exit();
        try{
            $validator = \Validator::make($request->all(), [
                'album_name' => 'required',
                'year' => 'required',
            ]);
            if($validator->fails()){
                session()->flash('status-error', 'Please fill in all fields!');
                return redirect()->route('album-create');
            }
            Album::create($request->all());
            session()->flash('status-success', 'Record created successfully!');
            return redirect()->route('album-create');
        }catch (\Exception $e) {
            session()->flash('status-error', 'Error creating the record.');
            return redirect()->route('album-create');
        }
    }


    public function edit($id)
    {
        try{
            $album = Album::findOrFail($id);
            $artists = Artist::orderBy('artist_name')->get();
            return view('album/edit', compact('artists', 'album'));
        }catch (\Exception $e) {
            session()->flash('status-error', 'No records found.');
            return redirect()->route('albums');
        }
    }

    public function update(Request  $request)
    {
        try{
            $validator = \Validator::make($request->all(), [
                'album_name' => 'required',
                'year' => 'required',
            ]);
            if($validator->fails()){
                session()->flash('status-error', 'Please fill in all fields!');
                return redirect()->route('album-edit', $request->id);
            }

            $album = Album::findOrFail($request->id);
            $album->update($request->all());
            session()->flash('status-success', 'Registration updated successfully!');
            return redirect()->route('album-edit', $request->id);
        }catch (\Exception $e) {
            session()->flash('status-error', 'Error updating the record.');
            return redirect()->route('album-edit', $request->id);
        }
    }

    public function destroy($id)
    {
        try{
            $album = Album::findOrFail($id);
            $album->delete($album);
            session()->flash('status-success', 'Record deleted successfully!');
            return redirect()->route('albums');
        }catch (\Exception $e) {
            session()->flash('status-error', 'There was an error deleting.');
            return redirect()->route('albums');
        }
    }

}

