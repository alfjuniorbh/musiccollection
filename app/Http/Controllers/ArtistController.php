<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;

class ArtistController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $artists = Artist::orderBy('artist_name')->get();
        return view('artist/index', compact('artists'));
    }

    public function create()
    {
        return view('artist/create');
    }

    public function store(Request  $request)
    {
        try{
            $validator = \Validator::make($request->all(), [
                'artist_name' => 'required',
                'twitter_handle' => 'required',
            ]);
            if($validator->fails()){
                session()->flash('status-error', 'Please fill in all fields!');
                return redirect()->route('artist-create');
            }
            Artist::create($request->all());
            session()->flash('status-success', 'Record created successfully!');
            return redirect()->route('artist-create');
        }catch (\Exception $e) {
            session()->flash('status-error', 'Error creating the record.');
            return redirect()->route('error');
        }
    }


    public function edit($id)
    {
        try{
            $artist = Artist::findOrFail($id);
            return view('artist/edit', compact('artist'));
        }catch (\Exception $e) {
            session()->flash('status-error', 'No records found.');
            return redirect()->route('error');
        }
    }

    public function update(Request  $request)
    {
        try{
            $validator = \Validator::make($request->all(), [
                'artist_name' => 'required',
                'twitter_handle' => 'required',
            ]);
            if($validator->fails()){
                session()->flash('status-error', 'Please fill in all fields!');
                return redirect()->route('artist-edit', $request->id);
            }

            $artist = Artist::findOrFail($request->id);
            $artist->update($request->all());
            session()->flash('status-success', 'Registration updated successfully!');
            return redirect()->route('artist-edit', $request->id);
        }catch (\Exception $e) {
            session()->flash('status-error', 'Error updating the record.');
            return redirect()->route('error');
        }
    }

    public function destroy($id)
    {
        try{
            $artist = Artist::findOrFail($id);
            $artist->delete($artist);
            session()->flash('status-success', 'Record deleted successfully!');
            return redirect()->route('home');
        }catch (\Exception $e) {
            session()->flash('status-error', 'There was an error deleting.');
            return redirect()->route('error');
        }
    }

}
