<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'artist_id', 'album_name', 'year'
    ];

    public function artist()
    {
        return $this->belongsTo('App\Artist', 'artist_id');
    }

}
