<p align="center"><img src="https://www.madesimplegroup.com/bundles/msgsite/imgs/msg-home.png" width="400"></p>

### Description

We would like you to create a very basic music collection app. This is just to check your OOP skills, general code organization and software architecture experience. Apply basic UX patterns to the UI, don't worry about the design.

The app must be simple, there's no need to create a registration page nor any other user management features. For data storage, feel free to use any relational database you want.

Once done, please send us the link to a public repository (we currently use gitlab) with the app. A deployed version that we could test is a bonus. 

If you're planning to use a framework (if you're not, you should :) ) please make sure you commit the framework code first and your changes later in separate commits. This is so we can clearly identify your code/changes. Do not rebase your code with the initial commit when you're pushing the framework code or it will be harder for us to look at just your code.

Feel free to come up with any requirement omitted from the specification below (eg: error messages, user-flows, etc). Just keep in mind that we're not looking for completeness of features nor design. You should not need to spend more than a couple of hours on this task, if you are, you're doing more than what we're asking :)


### Requirements

**Login**

- Create a basic login page with the fields `username` and `password`
- Once logged in the user should be able to access all internal pages of the music app
    - If a not-logged in user tries to access any internal page he should be redirected to the login page
- After a successful login the user should be redirected to the `Artists list` page
- A failed login attempt will send the user back to the login page with the error: `Sorry, we couldn't find an account with this username. Please check you're using the right username and try again.`

**Artists**

- Create 3 pages that the will let users list, add and edit artists.
- Each artist will contain the following fields:
    - `Artist name`
    - `Twitter handle`

**Albums**

- Create 3 pages that will let users list, add and edit albums.
- Each album will contain the following fields:
    - `Artist` let user select from the list of existing artists
    - `Album name`
    - `Year`
