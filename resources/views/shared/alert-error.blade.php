@if (session('status-error'))
<div class="row">
    <div class="alert alert-danger col-12" role="alert">
        {{ session('status-error') }}
    </div>
</div>
@endif