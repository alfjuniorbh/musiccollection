@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h1>Artist Edit: {{$artist->artist_name}}  | <small><a href="{{route('home')}}">List</a></small></h1></div>

                <div class="card-body">
                    @include('shared/alert-success')
                    @include('shared/alert-error')

                    <form method="POST" action="{{route('artist-update')}}">
                      @method('PUT')
                      @csrf

                      <input type="hidden" class="form-control" id="id" name="id" aria-describedby="id" value="{{$artist->id}}">
                  
                      <div class="form-group">
                        <label for="artist_name">Artist name</label>
                      <input type="text" class="form-control" id="artist_name" name="artist_name" aria-describedby="artist_name" value="{{$artist->artist_name}}" required>
                      </div>
                      <div class="form-group">
                        <label for="twitter_handle">Twitter handle</label>
                        <input type="text" class="form-control" id="twitter_handle" name="twitter_handle" value="{{$artist->twitter_handle}}" required>
                      </div>
    
                      
                      <a href="{{route('home')}}" class="btn btn-secondary">Back</a>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
