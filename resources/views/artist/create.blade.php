@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h1>Artist Create  | <small><a href="{{route('home')}}">List</a></small></h1></div>

                <div class="card-body">
                    @include('shared/alert-success')
                    @include('shared/alert-error')

                    <form method="POST" action="{{route('artist-store')}}">
                      @csrf
                  
                      <div class="form-group">
                        <label for="artist_name">Artist name</label>
                      <input type="text" class="form-control" id="artist_name" name="artist_name" aria-describedby="artist_name" value="{{old('artist_name')}}" required>
                      </div>
                      <div class="form-group">
                        <label for="twitter_handle">Twitter handle</label>
                        <input type="text" class="form-control" id="twitter_handle" name="twitter_handle" value="{{old('twitter_handle')}}" required>
                      </div>
    
                      
                      <a href="{{route('home')}}" class="btn btn-secondary">Back</a>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
