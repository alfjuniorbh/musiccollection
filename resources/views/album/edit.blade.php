@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h1>Album Edit: {{$album->album_name}}  | <small><a href="{{route('albums')}}">List</a></small></h1></div>

                <div class="card-body">
                    @include('shared/alert-success')
                    @include('shared/alert-error')

                    <form method="POST" action="{{route('album-update')}}">
                      @method('PUT')
                      @csrf

                      <input type="hidden" class="form-control" id="id" name="id" aria-describedby="id" value="{{$album->id}}">

                      <div class="form-group">
                        <label for="artist_name">Artist name</label>
                        <select class="form-control" name="artist_id">
                          @foreach($artists as $artist)
                          <option {{$album->artist_id == $artist->id ? 'selected' : ''}} value="{{$artist->id}}">{{$artist->artist_name}}</option>
                          @endforeach
                        </select>
                      </div>
                  
                      <div class="form-group">
                        <label for="album_name">Album name</label>
                      <input type="text" class="form-control" id="album_name" name="album_name" aria-describedby="album_name" value="{{$album->album_name}}" required>
                      </div>
                      <div class="form-group">
                        <label for="year">Year</label>
                        <input type="number" class="form-control" id="year" name="year" value="{{$album->year}}"   maxlength="4" required>
                      </div>
    
                      
                      <a href="{{route('albums')}}" class="btn btn-secondary">Back</a>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
