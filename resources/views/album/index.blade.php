@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h1>Albums | <small><a href="{{route('album-create')}}">Create</a></small></h1></div>

                <div class="card-body">
                    @include('shared/alert-success')
                    @include('shared/alert-error')

                    <table class="table table-hover table-sm">
                      <thead class="thead-dark">
                        <tr class="row">
                          <th class="col-sm-1 text-center">#</th>
                          <th class="col-sm-4 text-uppercase text-center">Artist name</th>
                          <th class="col-sm-3 text-uppercase text-center">Album name</th>
                          <th class="col-sm-1 text-uppercase text-center">Year</th>
                          <th class="col-sm-2 text-uppercase text-center">Created at</th>
                          <th class="col-sm-1 text-uppercase text-center">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($albums->count() > 0)
                        @foreach($albums as $album)
                            <tr class="row">
                              <td class="col-sm-1 text-center">{{$album->id}}</td>
                              <td class="col-sm-4">{{$album->artist->artist_name}}</td>
                              <td class="col-sm-3">{{$album->album_name}}</td>
                              <td class="col-sm-1">{{$album->year}}</td>
                              <td class="col-sm-2 text-center">{{$album->created_at->format('d/m/Y')}}</td>
                              <td class="col-sm-1 text-center">
                                <a href="{{route('album-edit', $album->id)}}"><i class="btn btn-sm btn-primary fas fa-edit"></i></a>
                                <a href="{{route('album-destroy', $album->id)}}"><i class="btn btn-sm btn-danger fas fa-trash-alt"></i></a>
                              </td>
                            </tr>                        
                        @endforeach
                      @else
                        <tr scope="row">
                          <td colspan="6"><p class="text-center">No results found</p></td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
