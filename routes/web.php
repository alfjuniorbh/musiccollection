<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@login')->name('login');

Auth::routes();

Route::get('/home', 'ArtistController@index')->name('dashboard');
Route::get('/artists', 'ArtistController@index')->name('home');
Route::get('/artist/destroy/{id}', 'ArtistController@destroy')->name('artist-destroy');
Route::get('/artist/create', 'ArtistController@create')->name('artist-create');
Route::post('/artist/store', 'ArtistController@store')->name('artist-store');
Route::get('/artist/edit/{id}', 'ArtistController@edit')->name('artist-edit');
Route::put('/artist/update', 'ArtistController@update')->name('artist-update');

Route::get('/albums', 'AlbumController@index')->name('albums');
Route::get('/album/destroy/{id}', 'AlbumController@destroy')->name('album-destroy');
Route::get('/album/create', 'AlbumController@create')->name('album-create');
Route::post('/album/store', 'AlbumController@store')->name('album-store');
Route::get('/album/edit/{id}', 'AlbumController@edit')->name('album-edit');
Route::put('/album/update', 'AlbumController@update')->name('album-update');
